# Asset Showcase

A template Unity project for showcasing assets. 

Easier than ever before! Just drag and drop.

## Features:

* URP with PostProcessing & AO
* UV View
* Wireframe View
* Textured View
* Tri Count

## Shortcuts:

| Key           | Action         |
|---------------|----------------|
| Tab         | Show / Hide UI |
| 1           | Textured View  |
| 2           | UV View        |
| 3           | Wireframe View |
| Spacebar    | Play / Pause   |
| Right Arrow | Next Asset     |
| Left Arrow  | Previous Asset |
| +           | Zoom In        |
| -           | Zoom Out       |

## Usage:


![AssetImportDemo](AssetImportDemo.gif)
1. Clone or download repo. Open in Unity 2019.4.14f1 or higher.
2. Import model into Unity. Textures need to be imported individually and are not packaged in the model.
3. Select the model and do the following in the Inspector:
    1. Under the model tab, mark `Read/Write Enabled` to `true`
    2. Under the materials tab, `Extract Materials`
4. Tweak extracted materials and assign any missing textures.
5. Drag Model/Prefab into scene under the `----Assets----` gameobject.
6. Use Unity Recorder to record the game view at any desired resolution. `Window > General > Recorder > Recorder Window`
![UnityRecorderDemo](AssetShowcaseDemo.gif)
