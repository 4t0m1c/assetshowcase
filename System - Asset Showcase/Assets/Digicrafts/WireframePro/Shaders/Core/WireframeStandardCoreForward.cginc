#ifndef UNITY_STANDARD_CORE_FORWARD_INCLUDED
#define UNITY_STANDARD_CORE_FORWARD_INCLUDED

#if defined(UNITY_NO_FULL_STANDARD_SHADER)
#   define UNITY_STANDARD_SIMPLE 1
#endif

#include "UnityStandardConfig.cginc"
#include "WireframeCore.cginc"

#if UNITY_STANDARD_SIMPLE
    #include "WireframeStandardCoreForwardSimple.cginc"
    [maxvertexcount(3)]
    void geomBase(triangle VertexOutputBaseSimple IN[3], inout TriangleStream<VertexOutputBaseSimple> triStream)
    {       
        IN[0].mass = float4(1,0,0,0);
        triStream.Append(IN[0]);
        IN[1].mass = float4(0,1,0,0);
        triStream.Append( IN[1]); 
        IN[2].mass = float4(0,0,1,0);
        triStream.Append(IN[2]);               
    }
    [maxvertexcount(3)]
    void geomAdd(triangle VertexOutputForwardAddSimple IN[3], inout TriangleStream<VertexOutputForwardAddSimple> triStream)
    {
        IN[0].mass = float4(1,0,0,0);
        triStream.Append(IN[0]);
        IN[1].mass = float4(0,1,0,0);
        triStream.Append( IN[1]); 
        IN[2].mass = float4(0,0,1,0);
        triStream.Append(IN[2]);               
    }
    VertexOutputBaseSimple vertBase (VertexInput v) { return vertForwardBaseSimple(v); }
    VertexOutputForwardAddSimple vertAdd (VertexInput v) { return vertForwardAddSimple(v); }
    half4 fragBase (VertexOutputBaseSimple i) : SV_Target { return fragForwardBaseSimpleInternal(i); }
    half4 fragAdd (VertexOutputForwardAddSimple i) : SV_Target { return fragForwardAddSimpleInternal(i); }
#else
    #include "WireframeStandardCore.cginc"
    [maxvertexcount(3)]
    void geomBase(triangle VertexOutputForwardBase IN[3], inout TriangleStream<VertexOutputForwardBase> triStream)
    {
        IN[0].mass = float4(1,0,0,0);
        triStream.Append(IN[0]);
        IN[1].mass = float4(0,1,0,0);
        triStream.Append( IN[1]); 
        IN[2].mass = float4(0,0,1,0);
        triStream.Append(IN[2]);               
    }   
    [maxvertexcount(3)]
    void geomAdd(triangle VertexOutputForwardAdd IN[3], inout TriangleStream<VertexOutputForwardAdd> triStream)
    {
        IN[0].mass = float4(1,0,0,0);
        triStream.Append(IN[0]);
        IN[1].mass = float4(0,1,0,0);
        triStream.Append( IN[1]); 
        IN[2].mass = float4(0,0,1,0);
        triStream.Append(IN[2]);               
    }
    VertexOutputForwardBase vertBase (VertexInput v) { return vertForwardBase(v); }
    VertexOutputForwardAdd vertAdd (VertexInput v) { return vertForwardAdd(v); }
    half4 fragBase (VertexOutputForwardBase i) : SV_Target { return fragForwardBaseInternal(i); }
    half4 fragAdd (VertexOutputForwardAdd i) : SV_Target { return fragForwardAddInternal(i); }
#endif

#endif // UNITY_STANDARD_CORE_FORWARD_INCLUDED
