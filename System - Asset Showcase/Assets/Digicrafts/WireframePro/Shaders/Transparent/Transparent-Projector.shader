//UNITY_SHADER_NO_UPGRADE
// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader "Digicrafts/Wireframe/Projector" {
Properties {	

	// Wireframe Properties
	[HDR]_WireframeColor ("Color", Color) = (1,1,1,1)
	_WireframeTex ("Texture", 2D) = "white" {}
	[Enum(UV0,0,UV1,1)] _WireframeUV ("UV Set for Texture", Float) = 0
	_WireframeSize ("Size", Range(0.0, 10.0)) = 1
	[Toggle(_WIREFRAME_LIGHTING)]_WireframeLighting ("Color affect by Light", Float) = 0
	[Toggle(_WIREFRAME_AA)]_WireframeAA ("Anti Aliasing", Float) = 0
	[Toggle]_WireframeDoubleSided ("2 Sided", Float) = 0
	_WireframeMaskTex ("Mask Texture", 2D) = "white" {}
	_WireframeTexAniSpeedX ("Speed X", Float) = 0
	_WireframeTexAniSpeedY ("Speed Y", Float) = 0
	[Toggle(_WIREFRAME_VERTEX_COLOR)]_WireframeVertexColor ("VertexColor", Float) = 0
	
	_ProjectorTex ("Projector Texture", 2D) = "white" {}
	_ProjectorFalloffTex ("Falloff Texture", 2D) = "white" {}

	_WireframeAlphaCutoff ("_WireframeAlphaCutoff", Range(0.0, 1.0)) = 0.5
	[HideInInspector] _WireframeAlphaMode ("__WireframeAlphaMode", Float) = 0
	[HideInInspector] _WireframeCull ("__WireframeCull", Float) = 2
}

SubShader {

	Tags { "RenderType"="Transparent" "Queue"="Transparent"}

	Pass { 

		Name "Wireframe"
		Cull [_WireframeCull]
		ZWrite Off
		ColorMask RGB
		Blend SrcAlpha OneMinusSrcAlpha 
		Offset -1, -1

		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma shader_feature _WIREFRAME_AA
			#pragma shader_feature _WIREFRAME_ALPHA_NORMAL _WIREFRAME_ALPHA_TEX_ALPHA _WIREFRAME_ALPHA_TEX_ALPHA_INVERT _WIREFRAME_ALPHA_MASK _WIREFRAME_ALPHA_MASK_INVERT
			#include "UnityCG.cginc"
			#include "../Core/WireframeCore.cginc"

			uniform sampler2D _ProjectorTex;
			uniform sampler2D _ProjectorFalloffTex;
			DC_WIREFRAME_PROJECTOR_VAR

			struct appdata {				
				float4 vertex : POSITION;
				#if _WIREFRAME_VERTEX_COLOR
				float4 color : COLOR0;
				#endif
				float2 uv0 : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				fixed2 uv4 : TEXCOORD3;
			};		
			struct v2f {
				float4 pos : SV_POSITION;
				DC_WIREFRAME_COORDS(0,1)	
				float4 projector : TEXCOORD2;
				float4 projectorFalloff : TEXCOORD3;
			};
			v2f vert (appdata v)
			{
				v2f o;
				#if UNITY_VERSION >= 540
					o.pos = UnityObjectToClipPos(v.vertex);
				#else
					o.pos = mul(UNITY_MATRIX_MVP,v.vertex);
				#endif				
				DC_WIREFRAME_TRANSFER_COORDS(o);
				//UNITY_SHADER_NO_UPGRADE
				o.projector = mul (_Projector, v.vertex);
				o.projectorFalloff = mul (_ProjectorClip, v.vertex);
				return o;
			}
			fixed4 frag (v2f i) : COLOR
			{						
				fixed4 projectorColor = tex2Dproj (_ProjectorTex, UNITY_PROJ_COORD(i.projector));
				fixed4 projectorFalloff = tex2Dproj (_ProjectorFalloffTex, UNITY_PROJ_COORD(i.projectorFalloff));		
				fixed4 c = fixed4(0,0,0,0);
				DC_APPLY_WIREFRAME(c.rgb,c.a,i)

				fixed4 res = lerp(fixed4(0,0,0,0), c, projectorColor.a*projectorFalloff.a);
				return res;
			}

		ENDCG
	}

}
CustomEditor "WireframeTransparentShaderGUI"
}
