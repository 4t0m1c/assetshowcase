using UnityEngine;
using UnityEditor;


using System.Collections.Generic;
using System;

namespace Digicrafts.WireframePro.Editor
{
    internal class AbstractLitShaderGUI : BaseShaderGUI
    {
        // Properties
        private UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.LitProperties litProperties;

        // collect properties from the material properties
        public override void FindProperties(MaterialProperty[] properties)
        {
            base.FindProperties(properties);
            litProperties = new UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.LitProperties(properties);
        }

        // material changed check
        public override void MaterialChanged(Material material)
        {
            if (material == null)
                throw new ArgumentNullException("material");

            SetMaterialKeywords(material, UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.SetMaterialKeywords);
        }

        // material main surface options
        public override void DrawSurfaceOptions(Material material)
        {
            if (material == null)
                throw new ArgumentNullException("material");

            // Use default labelWidth
            EditorGUIUtility.labelWidth = 0f;

            // Detect any changes to the material
            EditorGUI.BeginChangeCheck();
            if (litProperties.workflowMode != null)
            {
                DoPopup(UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.Styles.workflowModeText, litProperties.workflowMode, Enum.GetNames(typeof(UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.WorkflowMode)));
            }
            if (EditorGUI.EndChangeCheck())
            {
                foreach (var obj in blendModeProp.targets)
                    MaterialChanged((Material)obj);
            }
            base.DrawSurfaceOptions(material);
        }

        // material main surface inputs
        public override void DrawSurfaceInputs(Material material)
        {
            base.DrawSurfaceInputs(material);
            UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.Inputs(litProperties, materialEditor, material);
            DrawEmissionProperties(material, true);
            DrawTileOffset(materialEditor, baseMapProp);
        }

        // material main advanced options
        public override void DrawAdvancedOptions(Material material)
        {
            if (litProperties.reflections != null && litProperties.highlights != null)
            {
                EditorGUI.BeginChangeCheck();
                {
                    materialEditor.ShaderProperty(litProperties.highlights, UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.Styles.highlightsText);
                    materialEditor.ShaderProperty(litProperties.reflections, UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.Styles.reflectionsText);
                    EditorGUI.BeginChangeCheck();
                }
            }

            base.DrawAdvancedOptions(material);
        }

        public override void AssignNewShaderToMaterial(Material material, Shader oldShader, Shader newShader)
        {
            if (material == null)
                throw new ArgumentNullException("material");

            // _Emission property is lost after assigning Standard shader to the material
            // thus transfer it before assigning the new shader
            if (material.HasProperty("_Emission"))
            {
                material.SetColor("_EmissionColor", material.GetColor("_Emission"));
            }

            base.AssignNewShaderToMaterial(material, oldShader, newShader);

            if (oldShader == null || !oldShader.name.Contains("Legacy Shaders/"))
            {
                SetupMaterialBlendMode(material);
                return;
            }

            SurfaceType surfaceType = SurfaceType.Opaque;
            BlendMode blendMode = BlendMode.Alpha;
            if (oldShader.name.Contains("/Transparent/Cutout/"))
            {
                surfaceType = SurfaceType.Opaque;
                material.SetFloat("_AlphaClip", 1);
            }
            else if (oldShader.name.Contains("/Transparent/"))
            {
                // NOTE: legacy shaders did not provide physically based transparency
                // therefore Fade mode
                surfaceType = SurfaceType.Transparent;
                blendMode = BlendMode.Alpha;
            }
            material.SetFloat("_Surface", (float)surfaceType);
            material.SetFloat("_Blend", (float)blendMode);

            if (oldShader.name.Equals("Standard (Specular setup)"))
            {
                material.SetFloat("_WorkflowMode", (float)UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.WorkflowMode.Specular);
                Texture texture = material.GetTexture("_SpecGlossMap");
                if (texture != null)
                    material.SetTexture("_MetallicSpecGlossMap", texture);
            }
            else
            {
                material.SetFloat("_WorkflowMode", (float)UnityEditor.Rendering.Universal.ShaderGUI.LitGUI.WorkflowMode.Metallic);
                Texture texture = material.GetTexture("_MetallicGlossMap");
                if (texture != null)
                    material.SetTexture("_MetallicSpecGlossMap", texture);
            }

            MaterialChanged(material);
        }
    }
    // /// <summary>
    // /// 
    // /// </summary>
    // internal class WireframeLithaderGUI : WireframeShaderGUI
    // {

    //     private bool _inited = false;
    //     private float _foamEnable = -1;
    //     private bool _lightingEnable = false;

    //     private static Dictionary<string, ShaderEditorProperty> _materialProperties =
    //         new Dictionary<string, ShaderEditorProperty>(){

    //// Workflow
    //         {"_WorkflowMode",new ShaderEditorProperty("_WorkflowMode")},
    //         {"_Surface",new ShaderEditorProperty("Surface Type")},
    //         {"_Blend",new ShaderEditorProperty("_Blend")},
    //         {"_Cull",new ShaderEditorProperty("_Cull")},
    //         {"_AlphaClip",new ShaderEditorProperty("_AlphaClip")},
    //         {"_Cutoff",new ShaderEditorProperty("_Cutoff")},
    //         // Lighting           
    //         {"_SpecColor",new ShaderEditorProperty("Specular Color")},
    //         {"_SpecGlossMap",new ShaderEditorProperty("Specular Map")},            
    //         {"_Metallic",new ShaderEditorProperty("Metallic")},
    //         {"_MetallicGlossMap",new ShaderEditorProperty("Metallic")},
    //         {"_Smoothness",new ShaderEditorProperty("Smoothness")},
    //         {"_ReceiveShadows",new ShaderEditorProperty("Receive Shadows")},
    //         {"_SpecularHighlights",new ShaderEditorProperty("Specular Highlights")},
    //         {"_GlossyReflections",new ShaderEditorProperty("Glossy Reflections")},

    //         };

    //     override public void FindProperties(MaterialProperty[] props)
    //     {
    //         if (_inited)
    //         {

    //         }
    //         else
    //         {
    //             base.FindProperties(props);

    //             // Loop and find property
    //             foreach (KeyValuePair<string, ShaderEditorProperty> kvp in _materialProperties)
    //             {
    //                 kvp.Value.prop = FindProperty(kvp.Key, props, false);
    //             }
    //             // check if shader support lighting
    //             if (_materialProperties["_Metallic"].prop != null) _lightingEnable = true;
    //             _inited = true;
    //         }
    //     }

    //     private void ShaderProperty(MaterialEditor materialEditor, string name, int labelIndent = 0)
    //     {
    //         ShaderEditorProperty property = _materialProperties[name];
    //         if (property.prop != null)
    //             materialEditor.ShaderProperty(property.prop, property.title, labelIndent);
    //     }

    //     public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] properties)
    //     {
    //         FindProperties(properties); // MaterialProperties can be animated so we do not cache them but fetch them every event to ensure animated values are updated correctly

    //         // Header
    //         //EditorStyles.DrawSectionHeader("Lighting", "light-icon");

    //         Material material = (Material)materialEditor.target;

    //         // Workflow selection
    //         EditorGUI.BeginChangeCheck();
    //         BaseShaderGUI.DoPopup(new GUIContent("Workflow Mode"), _materialProperties["_WorkflowMode"].prop, Enum.GetNames(typeof(LitGUI.WorkflowMode)), materialEditor);
    //         //BaseShaderGUI.TextureColorProps(materialEditor, new GUIContent("213123"), _materialProperties["_SpecGlossMap"].prop, _materialProperties["_SpecColor"].prop);
    //         BaseShaderGUI.DoPopup(new GUIContent(_materialProperties["_Surface"].title), _materialProperties["_Surface"].prop, Enum.GetNames(typeof(BaseShaderGUI.SurfaceType)), materialEditor);
    //         if ((BaseShaderGUI.SurfaceType)material.GetFloat("_Surface") == BaseShaderGUI.SurfaceType.Transparent)
    //             BaseShaderGUI.DoPopup(new GUIContent("Blend Mode"), _materialProperties["_Blend"].prop, Enum.GetNames(typeof(BaseShaderGUI.BlendMode)), materialEditor);
    //         if (EditorGUI.EndChangeCheck())
    //         {
    //             LitGUI.SetMaterialKeywords((Material)materialEditor.target);
    //         }

    //         // Culling
    //         EditorGUI.BeginChangeCheck();
    //         MaterialProperty cullingProp = _materialProperties["_Cull"].prop;
    //         EditorGUI.showMixedValue = cullingProp.hasMixedValue;
    //         var culling = (BaseShaderGUI.RenderFace)cullingProp.floatValue;
    //         culling = (BaseShaderGUI.RenderFace)EditorGUILayout.EnumPopup(new GUIContent("Cull Mode"), culling);
    //         if (EditorGUI.EndChangeCheck())
    //         {
    //             //materialEditor.RegisterPropertyChangeUndo(Styles.cullingText.text);
    //             cullingProp.floatValue = (float)culling;
    //             material.doubleSidedGI = (BaseShaderGUI.RenderFace)cullingProp.floatValue != BaseShaderGUI.RenderFace.Front;
    //         }
    //         EditorGUI.showMixedValue = false;

    //         // Alpha
    //         MaterialProperty alphaClipProp = _materialProperties["_AlphaClip"].prop;
    //         MaterialProperty alphaCutoffProp = _materialProperties["_Cutoff"].prop;
    //         EditorGUI.BeginChangeCheck();
    //         EditorGUI.showMixedValue = alphaClipProp.hasMixedValue;
    //         var alphaClipEnabled = EditorGUILayout.Toggle(new GUIContent("Alpha Clip"), alphaClipProp.floatValue == 1);
    //         if (EditorGUI.EndChangeCheck())
    //             alphaClipProp.floatValue = alphaClipEnabled ? 1 : 0;
    //         EditorGUI.showMixedValue = false;

    //         if (alphaClipProp.floatValue == 1)
    //             materialEditor.ShaderProperty(alphaCutoffProp, new GUIContent("Alpha Cutoff"), 1);

    //         //EditorStyles.DrawHorizontalLine();
    //         ShaderProperty(materialEditor, "_Smoothness");

    //         MaterialProperty p = _materialProperties["_WorkflowMode"].prop;
    //         if (p == null ||
    //         (LitGUI.WorkflowMode)p.floatValue == LitGUI.WorkflowMode.Metallic)
    //         {
    //             ShaderProperty(materialEditor, "_Metallic");
    //             //materialEditor.TexturePropertySingleLine(new GUIContent("Metallic"), _materialProperties["_MetallicGlossMap"].prop, _materialProperties["_Metallic"].prop);                    
    //         }
    //         else
    //         {
    //             //BaseShaderGUI.TextureColorProps(materialEditor, new GUIContent("Specular"), _materialProperties["_SpecGlossMap"].prop, _materialProperties["_SpecColor"].prop);
    //             ShaderProperty(materialEditor, "_SpecColor");
    //         }


    //         ShaderProperty(materialEditor, "_SpecularHighlights");
    //         ShaderProperty(materialEditor, "_GlossyReflections");
    //         ShaderProperty(materialEditor, "_ReceiveShadows");


    //         EditorGUILayout.Space();

    //         WireframePropertiesGUI(materialEditor, material);

    //     }

    // }
}