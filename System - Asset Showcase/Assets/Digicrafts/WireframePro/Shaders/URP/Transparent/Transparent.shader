﻿//UNITY_SHADER_NO_UPGRADE
// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader "Digicrafts/Wireframe/URP/Transparent" {
	Properties {	

		// Wireframe Properties
		[HDR]_WireframeColor ("Color", Color) = (1,1,1,1)
		_WireframeTex ("Texture", 2D) = "white" {}
		[Enum(UV0,0,UV1,1)] _WireframeUV ("UV Set for Texture", Float) = 0
		_WireframeSize ("Size", Range(0.0, 10.0)) = 1
		[Toggle(_WIREFRAME_LIGHTING)]_WireframeLighting ("Color affect by Light", Float) = 0
		[Toggle(_WIREFRAME_AA)]_WireframeAA ("Anti Aliasing", Float) = 0
		[Toggle]_WireframeDoubleSided ("2 Sided", Float) = 0
		_WireframeMaskTex ("Mask Texture", 2D) = "white" {}
		_WireframeTexAniSpeedX ("Speed X", Float) = 0
		_WireframeTexAniSpeedY ("Speed Y", Float) = 0
		[Toggle(_WIREFRAME_VERTEX_COLOR)]_WireframeVertexColor ("VertexColor", Float) = 0

		_WireframeAlphaCutoff ("_WireframeAlphaCutoff", Range(0.0, 1.0)) = 0.5
		[HideInInspector] _WireframeAlphaMode ("__WireframeAlphaMode", Float) = 0
		[HideInInspector] _WireframeCull ("__WireframeCull", Float) = 2


		_EmissionLM ("Emission (Lightmapper)", Float) = 0 
		_Illum ("Illumin", 2D) = "white" {}

		_WireframeVertexTex ("Vertex Texture", 2D) = "white" {}
	}

	SubShader {

		Tags { "RenderType"="Transparent" "RenderPipeline" = "UniversalPipeline" "Queue"="Transparent"}

		Name "Wireframe"	  
		Cull [_WireframeCull]
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
			HLSLPROGRAM
                #pragma prefer_hlslcc gles
                #pragma exclude_renderers d3d11_9x
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 3.0
				#pragma shader_feature _EMISSION
				#pragma shader_feature _WIREFRAME_AA
				#pragma shader_feature _WIREFRAME_ALPHA_NORMAL _WIREFRAME_ALPHA_TEX_ALPHA _WIREFRAME_ALPHA_TEX_ALPHA_INVERT _WIREFRAME_ALPHA_MASK _WIREFRAME_ALPHA_MASK_INVERT

				#include "../Core/WireframeCore.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"


				uniform sampler2D _WireframeVertexTex;

				struct appdata {				
					float4 vertex : POSITION;
					#if _WIREFRAME_VERTEX_COLOR
					float4 color : COLOR0;
					#endif
					float2 uv : TEXCOORD0;
					float2 uv1 : TEXCOORD1;
					half2 uv4 : TEXCOORD3;
                    UNITY_VERTEX_INPUT_INSTANCE_ID
				};		

				struct v2f {
					float4 pos : SV_POSITION;
					DC_WIREFRAME_COORDS(0)				
				};

				v2f vert (appdata v)
				{
					v2f o;
					VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
                    o.pos=vertexInput.positionCS;
					DC_WIREFRAME_TRANSFER_COORDS(o);
					return o;
				}
				half4 frag (v2f i) : COLOR
				{								
					half4 c = half4(1,1,1,0);
					DC_APPLY_WIREFRAME(c.rgb,c.a,i)
					return c;
				}

			ENDHLSL
		}

	}
FallBack "VertexLit"
CustomEditor "WireframeTransparentShaderGUI"
}