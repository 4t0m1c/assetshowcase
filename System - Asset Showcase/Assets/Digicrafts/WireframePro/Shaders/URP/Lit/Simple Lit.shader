﻿Shader "Digicrafts/Wireframe/URP/Simple Lit"
{
    Properties
    {
         _BaseColor("Base Color", Color) = (0.5, 0.5, 0.5, 1)
        _BaseMap("Base Map (RGB) Smoothness / Alpha (A)", 2D) = "white" {}

        _Cutoff("Alpha Clipping", Range(0.0, 1.0)) = 0.5

        _SpecColor("Specular Color", Color) = (0.5, 0.5, 0.5, 0.5)
        _SpecGlossMap("Specular Map", 2D) = "white" {}
        [Enum(Specular Alpha,0,Albedo Alpha,1)] _SmoothnessSource("Smoothness Source", Float) = 0.0
        [ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0

        [HideInInspector] _BumpScale("Scale", Float) = 1.0
        [NoScaleOffset] _BumpMap("Normal Map", 2D) = "bump" {}

        _EmissionColor("Emission Color", Color) = (0,0,0)
        [NoScaleOffset]_EmissionMap("Emission Map", 2D) = "white" {}

        // Wireframe Properties
        [HDR]_WireframeColor ("Color", Color) = (1,1,1,1)
        _WireframeTex ("Texture", 2D) = "white" {}
        [Enum(UV0,0,UV1,1)] _WireframeUV ("UV Set for Texture", Float) = 0
        _WireframeSize ("Size", Range(0.0, 10.0)) = 1
        [Toggle(_WIREFRAME_LIGHTING)]_WireframeLighting ("Color affect by Light", Float) = 0
        [Toggle(_WIREFRAME_AA)]_WireframeAA ("Anti Aliasing", Float) = 0
        [Toggle]_WireframeDoubleSided ("2 Sided", Float) = 0
        _WireframeMaskTex ("Mask Texture", 2D) = "white" {}
        _WireframeTexAniSpeedX ("Speed X", Float) = 0
        _WireframeTexAniSpeedY ("Speed Y", Float) = 0
        [HDR]_WireframeEmissionColor("Color", Color) = (0,0,0)
        [Toggle(_WIREFRAME_VERTEX_COLOR)]_WireframeVertexColor ("VertexColor", Float) = 0

        _WireframeAlphaCutoff ("_WireframeAlphaCutoff", Range(0.0, 1.0)) = 0.5
        [HideInInspector] _WireframeAlphaMode ("__WireframeAlphaMode", Float) = 0
        [HideInInspector] _WireframeCull ("__WireframeCull", Float) = 2

        // Blending state
        [HideInInspector] _Surface("__surface", Float) = 0.0
        [HideInInspector] _Blend("__blend", Float) = 0.0
        [HideInInspector] _AlphaClip("__clip", Float) = 0.0
        [HideInInspector] _SrcBlend("__src", Float) = 1.0
        [HideInInspector] _DstBlend("__dst", Float) = 0.0
        [HideInInspector] _ZWrite("__zw", Float) = 1.0
        [HideInInspector] _Cull("__cull", Float) = 2.0

        [ToogleOff] _ReceiveShadows("Receive Shadows", Float) = 1.0
        
        // Editmode props
        [HideInInspector] _QueueOffset("Queue offset", Float) = 0.0
        [HideInInspector] _Smoothness("SMoothness", Float) = 0.5

        // ObsoleteProperties
        [HideInInspector] _MainTex("BaseMap", 2D) = "white" {}
        [HideInInspector] _Color("Base Color", Color) = (0.5, 0.5, 0.5, 1)
        [HideInInspector] _Shininess("Smoothness", Float) = 0.0
        [HideInInspector] _GlossinessSource("GlossinessSource", Float) = 0.0
        [HideInInspector] _SpecSource("SpecularHighlights", Float) = 0.0
    }

    SubShader
    {
        // With SRP we introduce a new "RenderPipeline" tag in Subshader. This allows to create shaders
        // that can match multiple render pipelines. If a RenderPipeline tag is not set it will match
        // any render pipeline. In case you want your subshader to only run in URP set the tag to
        // "UniversalPipeline"
        Tags{
            "RenderType" = "Opaque" 
            "RenderPipeline" = "UniversalPipeline" 
            "IgnoreProjector" = "True"
            }
        LOD 300

        // ------------------------------------------------------------------
        // Forward pass. Shades GI, emission, fog and all lights in a single pass.
        // Compared to Builtin pipeline forward renderer, URP forward renderer will
        // render a scene with multiple lights with less drawcalls and less overdraw.
        Pass
        {
            Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward" }

            // Use same blending / depth states as Standard shader
            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            Cull[_Cull]
            

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard SRP library
            // All shaders must be compiled with HLSLcc and currently only gles is not using HLSLcc by default
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0            

           // -------------------------------------
            // Material Keywords
            #pragma shader_feature _ALPHATEST_ON
            #pragma shader_feature _ALPHAPREMULTIPLY_ON
            #pragma shader_feature _ _SPECGLOSSMAP _SPECULAR_COLOR
            #pragma shader_feature _GLOSSINESS_FROM_BASE_ALPHA
            #pragma shader_feature _NORMALMAP
            #pragma shader_feature _EMISSION
            #pragma shader_feature _RECEIVE_SHADOWS_OFF

            // -------------------------------------
            // Universal Pipeline keywords
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile_fog

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            #pragma vertex LitPassVertexSimple
            #pragma fragment LitPassFragmentSimple

            //--------------------------------------
            // Wireframe
            #pragma shader_feature _WIREFRAME_LIGHTING
            #pragma shader_feature _WIREFRAME_AA
            #pragma shader_feature _WIREFRAME_ALPHA_NORMAL _WIREFRAME_ALPHA_TEX_ALPHA _WIREFRAME_ALPHA_TEX_ALPHA_INVERT _WIREFRAME_ALPHA_MASK _WIREFRAME_ALPHA_MASK_INVERT
            #pragma shader_feature _WIREFRAME_VERTEX_COLOR
            #include "../Core/WireframeCore.hlsl"
            
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/Shaders/SimpleLitInput.hlsl"            


            struct Attributes
            {
                float4 positionOS    : POSITION;
                float3 normalOS      : NORMAL;
                float4 tangentOS     : TANGENT;
                
                float2 uv            : TEXCOORD0;
                float2 uv1              : TEXCOORD1;                
                float2 uv4           : TEXCOORD3;

                #if _WIREFRAME_VERTEX_COLOR
                float4 color : COLOR0;
                #endif
                       
                UNITY_VERTEX_INPUT_INSTANCE_ID
                                
            };

            struct Varyings
            {
                float2 uv                       : TEXCOORD0;
                DECLARE_LIGHTMAP_OR_SH(lightmapUV, vertexSH, 1);
                float3 posWS                    : TEXCOORD2;    // xyz: posWS

#ifdef _NORMALMAP
                half4 normal                    : TEXCOORD3;    // xyz: normal, w: viewDir.x
                half4 tangent                   : TEXCOORD4;    // xyz: tangent, w: viewDir.y
                half4 bitangent                  : TEXCOORD5;    // xyz: bitangent, w: viewDir.z
#else
                half3  normal                   : TEXCOORD3;
                half3 viewDir                   : TEXCOORD4;
#endif

                half4 fogFactorAndVertexLight   : TEXCOORD6; // x: fogFactor, yzw: vertex light

#ifdef _MAIN_LIGHT_SHADOWS
                float4 shadowCoord              : TEXCOORD7;
#endif

                float4 positionCS               : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
                DC_WIREFRAME_COORDS(8)
            };

            void InitializeInputData(Varyings input, half3 normalTS, out InputData inputData)
            {
                inputData.positionWS = input.posWS;

            #ifdef _NORMALMAP
                half3 viewDirWS = half3(input.normal.w, input.tangent.w, input.bitangent.w);
                inputData.normalWS = TransformTangentToWorld(normalTS,
                    half3x3(input.tangent.xyz, input.bitangent.xyz, input.normal.xyz));
            #else
                half3 viewDirWS = input.viewDir;
                inputData.normalWS = input.normal;
            #endif

                inputData.normalWS = NormalizeNormalPerPixel(inputData.normalWS);
                viewDirWS = SafeNormalize(viewDirWS);

                inputData.viewDirectionWS = viewDirWS;
            #if defined(_MAIN_LIGHT_SHADOWS) && !defined(_RECEIVE_SHADOWS_OFF)
                inputData.shadowCoord = input.shadowCoord;
            #else
                inputData.shadowCoord = float4(0, 0, 0, 0);
            #endif
                inputData.fogCoord = input.fogFactorAndVertexLight.x;
                inputData.vertexLighting = input.fogFactorAndVertexLight.yzw;
                inputData.bakedGI = SAMPLE_GI(input.lightmapUV, input.vertexSH, inputData.normalWS);
            }

            ///////////////////////////////////////////////////////////////////////////////
            //                  Vertex and Fragment functions                            //
            ///////////////////////////////////////////////////////////////////////////////

            // Used in Standard (Simple Lighting) shader
            Varyings LitPassVertexSimple(Attributes v)
            {
                Varyings output = (Varyings)0;

                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, output);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.positionOS.xyz);
                VertexNormalInputs normalInput = GetVertexNormalInputs(v.normalOS, v.tangentOS);
                half3 viewDirWS = GetCameraPositionWS() - vertexInput.positionWS;
                half3 vertexLight = VertexLighting(vertexInput.positionWS, normalInput.normalWS);
                half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

                output.uv = TRANSFORM_TEX(v.uv, _BaseMap);
                output.posWS.xyz = vertexInput.positionWS;
                output.positionCS = vertexInput.positionCS;

            #ifdef _NORMALMAP
                output.normal = half4(normalInput.normalWS, viewDirWS.x);
                output.tangent = half4(normalInput.tangentWS, viewDirWS.y);
                output.bitangent = half4(normalInput.bitangentWS, viewDirWS.z);
            #else
                output.normal = NormalizeNormalPerVertex(normalInput.normalWS);
                output.viewDir = viewDirWS;
            #endif

                OUTPUT_LIGHTMAP_UV(v.lightmapUV, unity_LightmapST, output.lightmapUV);
                OUTPUT_SH(output.normal.xyz, output.vertexSH);

                output.fogFactorAndVertexLight = half4(fogFactor, vertexLight);

            #if defined(_MAIN_LIGHT_SHADOWS) && !defined(_RECEIVE_SHADOWS_OFF)
                output.shadowCoord = GetShadowCoord(vertexInput);
            #endif

                DC_WIREFRAME_TRANSFER_COORDS(output)       
                
                return output;
            }

            // Used for StandardSimpleLighting shader
            half4 LitPassFragmentSimple(Varyings i) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(i);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

                float2 uv = i.uv;
                half4 diffuseAlpha = SampleAlbedoAlpha(uv, TEXTURE2D_ARGS(_BaseMap, sampler_BaseMap));
                half3 diffuse = diffuseAlpha.rgb * _BaseColor.rgb;

                half alpha = diffuseAlpha.a * _BaseColor.a;
                AlphaDiscard(alpha, _Cutoff);
            #ifdef _ALPHAPREMULTIPLY_ON
                diffuse *= alpha;
            #endif

            #ifdef _WIREFRAME_LIGHTING
                DC_APPLY_WIREFRAME(diffuse,alpha,i)
            #endif

                half3 normalTS = SampleNormal(uv, TEXTURE2D_ARGS(_BumpMap, sampler_BumpMap));
                half3 emission = SampleEmission(uv, _EmissionColor.rgb, TEXTURE2D_ARGS(_EmissionMap, sampler_EmissionMap));
                half4 specular = SampleSpecularSmoothness(uv, alpha, _SpecColor, TEXTURE2D_ARGS(_SpecGlossMap, sampler_SpecGlossMap));
                half smoothness = specular.a;

                InputData inputData;
                InitializeInputData(i, normalTS, inputData);

                half4 color = UniversalFragmentBlinnPhong(inputData, diffuse, specular, smoothness, emission, alpha);
                color.rgb = MixFog(color.rgb, inputData.fogCoord);

            #ifndef _WIREFRAME_LIGHTING
                DC_APPLY_WIREFRAME(color.rgb,alpha,i)
            #endif
                
                return color;
            };
            ENDHLSL
        }
        
        UsePass "Universal Render Pipeline/Simple Lit/ShadowCaster"        
        UsePass "Universal Render Pipeline/Simple Lit/DepthOnly"        
        UsePass "Universal Render Pipeline/Simple Lit/Meta"
    }

    FallBack "Hidden/InternalErrorShader"
    
//CustomEditor "UnityEditor.Rendering.URP.ShaderGUI.LitShader"
CustomEditor "Digicrafts.WireframePro.Editor.WireframeSimpleLitShaderGUI"
}