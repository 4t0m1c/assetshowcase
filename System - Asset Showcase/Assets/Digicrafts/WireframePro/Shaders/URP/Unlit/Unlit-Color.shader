﻿//UNITY_SHADER_NO_UPGRADE
// Unlit shader. Simplest possible colored shader.
// - no lighting
// - no lightmap support
// - no texture

Shader "Digicrafts/Wireframe/URP/Unlit/Color" {
Properties {
	
	_Color ("Main Color", Color) = (0.5,0.5,0.5,1)

	// Wireframe Properties
	[HDR]_WireframeColor ("Color", Color) = (1,1,1,1)
	_WireframeTex ("Texture", 2D) = "white" {}
	[Enum(UV0,0,UV1,1)] _WireframeUV ("UV Set for Texture", Float) = 0
	_WireframeSize ("Size", Range(0.0, 10.0)) = 1
	[Toggle(_WIREFRAME_LIGHTING)]_WireframeLighting ("Color affect by Light", Float) = 0
	[Toggle(_WIREFRAME_AA)]_WireframeAA ("Anti Aliasing", Float) = 0
	[Toggle]_WireframeDoubleSided ("2 Sided", Float) = 0
	_WireframeMaskTex ("Mask Texture", 2D) = "white" {}
	_WireframeTexAniSpeedX ("Speed X", Float) = 0
	_WireframeTexAniSpeedY ("Speed Y", Float) = 0
	[Toggle(_WIREFRAME_VERTEX_COLOR)]_WireframeVertexColor ("VertexColor", Float) = 0

	_WireframeAlphaCutoff ("_WireframeAlphaCutoff", Range(0.0, 1.0)) = 0.5
	[HideInInspector] _WireframeAlphaMode ("__WireframeAlphaMode", Float) = 0
	[HideInInspector] _WireframeCull ("__WireframeCull", Float) = 2
}

SubShader {
	Tags { 
		"RenderType"="Opaque" 
		"RenderPipeline" = "UniversalPipeline" 
		}
	LOD 100
	Cull [_WireframeCull]
	
	Pass {  
		HLSLPROGRAM
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_fog
            #pragma multi_compile_instancing
			#pragma shader_feature _WIREFRAME_LIGHTING
			#pragma shader_feature _WIREFRAME_AA
			#pragma shader_feature _WIREFRAME_ALPHA_NORMAL _WIREFRAME_ALPHA_TEX_ALPHA _WIREFRAME_ALPHA_TEX_ALPHA_INVERT _WIREFRAME_ALPHA_MASK _WIREFRAME_ALPHA_MASK_INVERT
			#pragma shader_feature _WIREFRAME_VERTEX_COLOR
            			
			uniform half4 _Color;
																					
			#include "../Core/WireframeCore.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

			struct appdata {
				float4 vertex : POSITION;
				#if _WIREFRAME_VERTEX_COLOR
				float4 color : COLOR0;
				#endif
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD1;
				float2 uv4 : TEXCOORD3;
                UNITY_VERTEX_INPUT_INSTANCE_ID
			};
		
			struct v2f {
				float4 pos : SV_POSITION;				
				DC_WIREFRAME_COORDS(0)
				float4 positionWSAndFogFactor   : TEXCOORD2; // xyz: positionWS, w: vertex fog factor
			};
			
			v2f vert (appdata v)
			{
				v2f o;
				VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
                o.pos=vertexInput.positionCS;
				 // Computes fog factor per-vertex.
                float fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
                o.positionWSAndFogFactor = float4(vertexInput.positionWS, fogFactor);                
				DC_WIREFRAME_TRANSFER_COORDS(o);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 c = _Color;				
//				UNITY_OPAQUE_ALPHA(c.a);
				DC_APPLY_WIREFRAME(c.rgb,c.a,i)
				float fogFactor = i.positionWSAndFogFactor.w;
                c.rgb = MixFog(c.rgb, fogFactor);
				return c;				
			}
		ENDHLSL
	}
}
CustomEditor "WireframeGeneralShaderGUI"
}
