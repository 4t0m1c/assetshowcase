﻿//UNITY_SHADER_NO_UPGRADE
Shader "Digicrafts/Wireframe/URP/CartoonOutline"
{
	Properties
	{			
		_MainTex ("Texture", 2D) = "white" {}
		_OutlineColor ("Outline Color", Color) = (0,0,0,0)
		_OutlineWidth ("Outline Width", Range (0, 10)) = 0.5
		[Toggle] _Shade ("Eanble", Float) = 1
		_ShadeTex ("Texture", CUBE) = "gray" {}
      	_ShadePower ("Power",Range(0,1)) = 1
		_ShadeColor ("Shading Color", Color) = (1,1,1,1)
		[Toggle] _Shadow ("Eanble", Float) = 0
		_ShadowPower ("Shadow Power",Range(0,1)) = 0.2
		[Toggle] _Diffuse ("Diffuse", Float) = 0
		_DiffusePower ("Power",Range(0,1)) = 1
		[Toggle] _Ambient ("Ambient", Float) = 0
		_AmbientPower ("Power",Range(0,1)) = 1

		// Wireframe Properties
		[HDR]_WireframeColor ("Color", Color) = (1,1,1,1)
		_WireframeTex ("Texture", 2D) = "white" {}
		[Enum(UV0,0,UV1,1)] _WireframeUV ("UV Set for Texture", Float) = 0
		_WireframeSize ("Size", Range(0.0, 10.0)) = 1
		[Toggle(_WIREFRAME_LIGHTING)]_WireframeLighting ("Color affect by Light", Float) = 0
		[Toggle(_WIREFRAME_AA)]_WireframeAA ("Anti Aliasing", Float) = 0
		[Toggle]_WireframeDoubleSided ("2 Sided", Float) = 0
		_WireframeMaskTex ("Mask Texture", 2D) = "white" {}
		_WireframeTexAniSpeedX ("Speed X", Float) = 0
		_WireframeTexAniSpeedY ("Speed Y", Float) = 0
		[Toggle(_WIREFRAME_VERTEX_COLOR)]_WireframeVertexColor ("VertexColor", Float) = 0

		_WireframeAlphaCutoff ("_WireframeAlphaCutoff", Range(0.0, 1.0)) = 0.5
		[HideInInspector] _WireframeAlphaMode ("__WireframeAlphaMode", Float) = 0
		[HideInInspector] _WireframeCull ("__WireframeCull", Float) = 2

		[Toggle(_RECEIVE_SHADOWS_OFF)]_ReceiveShadows("Receive Shadows", Float) = 1.0

	}
	SubShader
	{	
		Tags { 
		"RenderType" = "Opaque" 
		"RenderPipeline" = "UniversalPipeline" 
		"IgnoreProjector" = "True"
		}	
		// Outline pass
		Pass
		{
            Name "OUTLINE"
            Cull Front                       	           	
           
            HLSLPROGRAM
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x

            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #pragma vertex vert
            #pragma fragment frag						

			uniform half _OutlineWidth;			
 			uniform half4 _OutlineColor;

           	struct appdata
			{
				float4 vertex : POSITION;				
				float3 normal : NORMAL;
			};
            struct v2f
            {
                float4 pos : POSITION;
                float4 color : COLOR;
                //UNITY_FOG_COORDS(0)
            };
			inline float2 TransformViewToProjection (float2 v) {
				return mul((float2x2)UNITY_MATRIX_P, v);
			}
            v2f vert(appdata v)
            {
                v2f o;
				if(_OutlineWidth!=0){	                					
                    VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
                    o.pos=vertexInput.positionCS;					
	                float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	                float2 offset = normalize(TransformViewToProjection(norm.xy));
	                float ortho = 100;
	                float persp = 10;               
	                if(UNITY_MATRIX_P[3][3]==0){
	                	o.pos.xy += offset  *  _OutlineWidth / persp;
	                }else{
	                	o.pos.xy += offset * _OutlineWidth  / ortho;
	            	}         	
				} else {
					o.pos = half4(0,0,0,0);
	                o.color = _OutlineColor;    
				}
				//UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
           
            half4 frag(v2f i) :COLOR
            {          
        		half4 c = half4(i.color.rgb,i.color.a);
        		//UNITY_APPLY_FOG(i.fogCoord,c);
            	return c;
            }
            ENDHLSL
        }
        // base
		Pass
		{
			Name "BASE"
			Tags { 
				"LightMode"="UniversalForward"
				"RenderType"="Opaque" 
			}
			LOD 100
			Cull [_WireframeCull]

			HLSLPROGRAM
			#pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0 
			// make fog work
			#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
			#pragma multi_compile_fog

 			#pragma shader_feature _RECEIVE_SHADOWS_OFF
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

			#pragma shader_feature _DIFFUSE_ON
        	#pragma shader_feature _AMBIENT_ON
        	#pragma shader_feature _SHADOW_ON
        	#pragma shader_feature _SHADE_ON
        	#pragma shader_feature _WIREFRAME_LIGHTING
        	#pragma shader_feature _WIREFRAME_AA
			#pragma shader_feature _WIREFRAME_ALPHA_NORMAL _WIREFRAME_ALPHA_TEX_ALPHA _WIREFRAME_ALPHA_TEX_ALPHA_INVERT _WIREFRAME_ALPHA_MASK _WIREFRAME_ALPHA_MASK_INVERT
			#pragma shader_feature _WIREFRAME_VERTEX_COLOR
			
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "../Core/WireframeCore.hlsl"

			struct appdata
			{
				float4 vertex : POSITION;
				#if _WIREFRAME_VERTEX_COLOR
				float4 color : COLOR0;
				#endif
				float2 uv : TEXCOORD0;
				float2 uv1     : TEXCOORD1;
				float3 normalOS     : NORMAL;
                float4 tangentOS    : TANGENT;
				half2 uv4 : TEXCOORD3;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvLM                     : TEXCOORD1;
				float4 pos : SV_POSITION;
				half3 objNormal : TEXCOORD2;
				half3 worldNormal : TEXCOORD3;

				#if _DIFFUSE_ON
				half3 diff : COLOR1;
				#endif

				#if _AMBIENT_ON
                half3 ambient : COLOR2;
                #endif

				#if _SHADOW_ON
				// SHADOW_COORDS(3)
				float4 shadowCoord              : TEXCOORD7;
                #endif

                #if _SHADE_ON
				float3 cubenormal : TEXCOORD4;
				#endif

				//UNITY_FOG_COORDS(5)
				DC_WIREFRAME_COORDS(6)	
			};


			sampler2D _MainTex;
			float4 _MainTex_ST;

			#if _DIFFUSE_ON
			uniform half4 _DiffuseColor;
			uniform float _DiffusePower;
			#endif

			#if _AMBIENT_ON
			uniform float _AmbientPower;
			#endif

			#if _SHADOW_ON
			uniform half4 _ShadowColor;
			uniform half _ShadowPower;
			#endif

			#if _SHADE_ON
			samplerCUBE _ShadeTex;
         	uniform half _ShadePower;
			uniform half4 _ShadeColor;			 
         	uniform half _SpecToonEffectPower;
			#endif

			v2f vert (appdata v)
			{
				v2f o;
				VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
                VertexNormalInputs vertexNormalInput = GetVertexNormalInputs(v.normalOS, v.tangentOS);
                o.pos=vertexInput.positionCS;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvLM = v.uv1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
				o.objNormal = v.normalOS;
				o.worldNormal = vertexNormalInput.normalWS;

				#if _SHADOW_ON
					// TRANSFER_SHADOW(o)
					o.shadowCoord = GetShadowCoord(vertexInput);
					Light mainLight = GetMainLight(o.shadowCoord);
				#else
					Light mainLight = GetMainLight();	
				#endif

				#if _SHADE_ON
					o.cubenormal = mul (UNITY_MATRIX_MV, float4(v.normalOS,0));
				#endif

				#if _DIFFUSE_ON
					// half nl = max(0, dot(o.worldNormal, _WorldSpaceLightPos0.xyz));
					half nl = saturate(dot(o.worldNormal, mainLight.direction));
					o.diff = nl * mainLight.color;
				#endif

				#if _AMBIENT_ON
					o.ambient = SampleSH(half4(o.worldNormal,1)) * _AmbientPower;
				#endif				

				//UNITY_TRANSFER_FOG(o,o.pos);
				DC_WIREFRAME_TRANSFER_COORDS(o);

				return o;
			}			
			half4 frag (v2f i) : SV_Target
			{				
				half4 c = tex2D(_MainTex, i.uv);

				#if _WIREFRAME_LIGHTING
				DC_APPLY_WIREFRAME(c.rgb,c.a,i)
				#endif

				#if _SHADOW_ON				
					Light mainLight = GetMainLight(i.shadowCoord);
					half shadow = clamp(mainLight.shadowAttenuation+1-_ShadowPower,0,1);
					#if _SHADOWTOONEFFECT_ON
					shadow = round(shadow*_ShadowToonEffectPower)/_ShadowToonEffectPower;
					#endif
				#else
					Light mainLight = GetMainLight();
					half shadow = 1;
				#endif
								           
				#if _SHADE_ON
					half4 cube = clamp(texCUBE(_ShadeTex, i.cubenormal),(1-_ShadePower),1);
					half3 sc = lerp(_ShadeColor.rgb*_ShadeColor.a, half3(1,1,1), saturate(cube.rgb));
					c.rgb = cube.rgb*c.rgb*sc;
	            #endif
	            	                            
                #if _DIFFUSE_ON && _AMBIENT_ON
					half3 df = lerp(half3(1,1,1),i.diff,_DiffusePower);
                	c.rgb=c.rgb*df*shadow+i.ambient;					
				#else
					#if _DIFFUSE_ON
						half3 df = lerp(half3(1,1,1),i.diff,_DiffusePower);
						c.rgb*=df*shadow;
					#else 
						#if _AMBIENT_ON
							c.rgb=c.rgb*shadow+i.ambient;
						#else
							c.rgb=c.rgb*shadow;
						#endif
					#endif
				#endif									

				#if _SHADOW_ON
					c.rgb = c.rgb + (1-shadow)*_ShadowColor.rgb*_ShadowPower;
				#endif

				#ifndef _WIREFRAME_LIGHTING
				DC_APPLY_WIREFRAME(c.rgb,c.a,i)
				#endif

				//UNITY_APPLY_FOG(i.fogCoord,c);

				return half4(c.rgb,c.a);				
			}
			ENDHLSL
		}


		// shadow pass
        
	    
	}
CustomEditor "WireframeToonShaderGUI"
}


//Pass {
//    Name "ShadowCaster"
//    Tags { "LightMode" = "ShadowCaster" }
   
//    HLSLPROGRAM
//#pragma vertex vert
//#pragma fragment frag
//#pragma multi_compile_shadowcaster
//# include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

//    struct v2f
//{
//    V2F_SHADOW_CASTER;
//    };

//v2f vert(appdata_base v)
//{
//    v2f o;
//    TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
//        return o;
//}

//float4 frag(v2f i) : SV_Target
//    {
//        SHADOW_CASTER_FRAGMENT(i)
//    }
//    ENDHLSL          
//}