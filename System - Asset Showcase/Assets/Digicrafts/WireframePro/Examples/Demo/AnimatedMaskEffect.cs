﻿using UnityEngine;
using System.Collections;

public class AnimatedMaskEffect : MonoBehaviour {

	private Quaternion _startRotation;
	private Material _material;
	private float _value = 0;
	private int _direction = 0;

	// Use this for initialization
	void Start () {

		_startRotation = gameObject.transform.rotation;
		_material = this.GetComponent<Renderer>().material;		
	}
	
	// Update is called once per frame
	void Update () {
	
			Vector3 v = gameObject.transform.rotation.eulerAngles;
			v.y += 1;
			_startRotation.eulerAngles=v;
			gameObject.transform.rotation =_startRotation;

			_material.SetFloat("_Cutoff",_value);
			_material.SetFloat("_WireframeAlphaCutoff",1-_value);


			if(_direction==0){

				if(_value>=1){
					_direction=1;
					_value-=0.01f;
				} else {
					_value+=0.01f;
				}

			} else {

				if(_value<=0){
					_direction=0;
					_value+=0.01f;
				} else {
					_value-=0.01f;
				}
			}			

	}
}
