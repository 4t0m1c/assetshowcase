﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonToggle : MonoBehaviour {

	public GameObject _on;
	public GameObject _off;

	public void ToggleButton (bool _mode) {
		if (_mode) {
			_on.SetActive (true);
			_off.SetActive (false);
		} else {
			_on.SetActive (false);
			_off.SetActive (true);
		}
	}

}