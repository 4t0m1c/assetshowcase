﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MeshInfoDisplay : MonoBehaviour {

	/*
		Take model and create sub hierarchy of duplciate model with various material changes
		
		Drake_Low >>
			Drake_Low Parent
				- Drake_Low Tex
				- Drake_Low UV
				- Drake_Low Wireframe

		Switch through sub models by current view option
	*/

	[System.Serializable]
	public enum displayMode {
		Textured = 0,
		UV = 1,
		Wireframe = 2
	}

	[Header ("Input")]
	[SerializeField] Text _displayTriCountText;
	[SerializeField] Transform assetsParent;
	[SerializeField] Material UVMaterial;
	[SerializeField] Material wireframeMaterial;
	[SerializeField] float rotateSpeed = 20;
	[SerializeField] float torqueMultiplier = 50;
	[SerializeField] float zoomMultiplier = 5;
	[SerializeField] Canvas UICanvas;

	[Header ("Dynamic Input")]
	[SerializeField] displayMode _displayMode;
	[SerializeField] List<GameObject> _assets = new List<GameObject> ();
	[SerializeField] GameObject targetMeshObject;
	[SerializeField] MeshFilter[] _meshFilters;

	[Header ("Output")]
	public int triCount;
	public int assetIndex = 0;
	bool rotateObject = false;

	float lastMousePositionX;
	float lastDeltaX;
	bool rotatingChar;
	Transform _cameraTransform;
	Vector3 targetScale = Vector3.one;

	void Start () {
		assetIndex = 0;
		GenerateHierarchy ();

		if (assetsParent != null) {
			foreach (Transform child in assetsParent) {
				_assets.Add (child.gameObject);
				child.gameObject.SetActive (false);
			}
		}
		if (_assets.Count > 0) SetNewAsset (_assets[0]);

		_cameraTransform = Camera.main.transform.parent;
	}

	void GenerateHierarchy () {
		for (int i = 0; i < assetsParent.childCount; i++) {
			Transform child = assetsParent.GetChild (i);
			child.gameObject.SetActive (true);
			GameObject goParent = new GameObject ();
			goParent.name = child.name + "_Parent";
			goParent.transform.SetParent (assetsParent);
			goParent.transform.SetSiblingIndex (i);
			child.SetParent (goParent.transform);

			//Duplicate model and replace materials
			GameObject UVGo = Instantiate (child.gameObject, goParent.transform);
			UVGo.name += "_UV";
			ReplaceMats (UVGo, UVMaterial);
			UVGo.SetActive (false);

			GameObject wireframeGO = Instantiate (child.gameObject, goParent.transform);
			wireframeGO.name += "_Wireframe";
			ReplaceMats (wireframeGO, wireframeMaterial);
			wireframeGO.AddComponent<MeshUpdaterShaderGraph> ();
			wireframeGO.SetActive (false);

			child.name += "_Textured";
		}
	}

	void ReplaceMats (GameObject go, Material mat) {
		List<Renderer> rends = new List<Renderer> ();
		rends.AddRange (go.GetComponentsInChildren<Renderer> ().ToList ());

		foreach (var rend in rends) {
			Material[] mats = rend.materials;
			for (int i = 0; i < rend.materials.Length; i++) {
				mats[i] = mat;
			}
			rend.materials = mats;
		}
	}

	void Update () {
		float scrollDelta = Input.mouseScrollDelta.y;

		//Keyboard shortcuts
		if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)) {
			ChangeDisplayMode (0);
		}
		if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)) {
			ChangeDisplayMode (1);
		}
		if (Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Keypad3)) {
			ChangeDisplayMode (2);
		}
		if (Input.GetKeyDown (KeyCode.Space)) {
			RotateObject (!rotateObject);
		}
		if (Input.GetKeyDown (KeyCode.KeypadPlus)) {
			scrollDelta = 1;
		}
		if (Input.GetKeyDown (KeyCode.KeypadMinus)) {
			scrollDelta = -1;
		}
		if (Input.GetKeyDown (KeyCode.Tab)) {
			//Hide UI
			UICanvas.enabled = !UICanvas.enabled;
		}
		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			NextAsset(true);
		}
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			NextAsset(false);
		}


		//Zoom
		if (scrollDelta != 0) {
			float _scale = targetScale.x;
			_scale -= scrollDelta * Time.deltaTime * zoomMultiplier;
			_scale = Mathf.Clamp (_scale, 0.2f, 1f);
			targetScale = new Vector3 (_scale, _scale, _scale);
		}

		_cameraTransform.localScale = Vector3.Lerp (_cameraTransform.localScale, targetScale, Time.deltaTime * 2);

		int numTriangles = 0;
		if (_meshFilters.Length > 0) {
			foreach (MeshFilter filter in _meshFilters) {
				numTriangles += filter.mesh.triangles.Length / 3;
			}
		}
		triCount = numTriangles;
		_displayTriCountText.text = triCount.ToString ();

		if (rotateObject) {
			targetMeshObject.transform.Rotate (Vector3.up * Time.deltaTime * rotateSpeed, Space.World);
		}

		RotateAsset ();
	}

	public void SetNewAsset (GameObject _go) {
		if (targetMeshObject != null) {
			targetMeshObject.SetActive (false);
			_go.transform.rotation = targetMeshObject.transform.rotation;
		}
		_meshFilters = _go.GetComponentsInChildren<MeshFilter> ();
		_go.SetActive (true);
		targetMeshObject = _go;

		ChangeDisplayMode ((int) _displayMode);
	}

	public void NextAsset (bool _next) {
		if (_next) {
			assetIndex++;
			if (assetIndex == _assets.Count) {
				assetIndex = 0;
			}
		} else {
			assetIndex--;
			if (assetIndex < 0) {
				assetIndex = _assets.Count - 1;
			}
		}
		SetNewAsset (_assets[assetIndex]);
	}

	public void RotateObject (bool _on) {
		rotateObject = _on;
	}

	void RotateAsset () {
		if (rotatingChar) {
			if (Input.GetMouseButton (0)) {
				//Using Mouse Input
				lastDeltaX = lastMousePositionX - Input.mousePosition.x;
				targetMeshObject.transform.Rotate (0, lastDeltaX * Time.deltaTime * rotateSpeed, 0, Space.World);
				lastMousePositionX = Input.mousePosition.x;

			}
		} else if (lastDeltaX != 0) {
			targetMeshObject.transform.Rotate (new Vector3 (0, lastDeltaX, 0) * Time.deltaTime * torqueMultiplier, Space.World);
			if (lastDeltaX > 0.25f) {
				lastDeltaX -= 0.25f;
			} else if (lastDeltaX < -0.25f) {
				lastDeltaX += 0.25f;
			} else {
				lastDeltaX = 0;
			}
		}
	}

	public void BeginCharRotate () {
		//Reset last vars
		lastMousePositionX = Input.mousePosition.x;

		rotatingChar = true;
	}

	public void EndCharRotate () {
		rotatingChar = false;
	}

	public void ChangeDisplayMode (int _index) {
		foreach (Transform child in targetMeshObject.transform) {
			child.gameObject.SetActive (false);
		}
		_displayMode = (displayMode) _index;
		targetMeshObject.transform.GetChild (_index).gameObject.SetActive (true);
	}
}